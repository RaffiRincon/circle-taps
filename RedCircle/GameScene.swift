//
//  GameScene.swift
//  RedCircle
//
//  Created by Rafael Rincon on 9/26/15.
//  Copyright (c) 2015 Rafael Rincon. All rights reserved.
//

import SpriteKit
import SceneKit

protocol GameSceneDelegate {
    func scored()
    func missed()
}

class GameScene: SKScene {
    
    var circle: SKShapeNode = SKShapeNode()
    
    var gameSceneDelegate: GameSceneDelegate?
    
    var center = CGPoint()
    var screenHieght = CGFloat()
    var screenWidth = CGFloat()
    var screenRect = CGRect()
    var gameHasNotStarted = true
    var parentViewController: GameViewController?
    var speedMultiplier = CGFloat()
    var timer = NSTimer()
    var circleRadius = CGFloat()
    
    override func didMoveToView(view: SKView) {
        physicsWorld.gravity = CGVectorMake(0, 0)
        backgroundColor =  parentViewController!.view.backgroundColor!
        screenWidth = frame.height * (UIDevice().model.containsString("iPhone") ? 9 / 16 : 3 / 4)
        screenRect.size.height = frame.height
        screenRect.size.width = screenWidth//frame.height * (UIDevice().model.containsString("iPhone") ? 9 / 16 : 3 / 4)
        screenRect.origin.x = (frame.width / 2) - (screenWidth / 2)
        screenRect.origin.y = 0
        speedMultiplier = screenWidth * (3 / 4)
        print("scale = ", UIScreen.mainScreen().scale)
        physicsBody = SKPhysicsBody(edgeLoopFromRect: screenRect)
        physicsBody?.friction = 0.0
        physicsBody?.restitution = 1.0
        physicsBody?.linearDamping = 0.0
        print("screenrect: \n\(screenRect)")
        name = "Main SKScene"
        
        //setup properties
        circleRadius = screenRect.width / 8
        
        //format play button
//        parentViewController?.leaderboardButton.frame.size.width = circleRadius * 2
//        parentViewController?.leaderboardButton.frame.size.height = (parentViewController?.leaderboardButton.frame.width)!
//        parentViewController?.leaderboardButton.layer.cornerRadius = circleRadius
        parentViewController?.viewDidLayoutSubviews()
        
        //setup circle
        circle = SKShapeNode(circleOfRadius: circleRadius)
        circle.physicsBody = SKPhysicsBody(circleOfRadius: circleRadius)
        circle.setScale(CGFloat(1.0))
        circle.antialiased = true
//        circle.fillColor = UIColor.blackColor()
        circle.fillColor = UIColor.darkGrayColor()
        circle.strokeColor = UIColor.clearColor()
        circle.alpha = 1.0
        
        circle.position = CGPoint(x: frame.midX, y: frame.midY)
        circle.physicsBody?.dynamic = true
        circle.physicsBody?.allowsRotation = false
        circle.physicsBody?.friction = 0
        circle.physicsBody?.restitution = 1.0
        circle.physicsBody?.linearDamping = 0
        circle.physicsBody?.contactTestBitMask = 16
        circle.physicsBody?.collisionBitMask = 16
        circle.name = "circle"
        
        addChild(circle)
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        for touch in touches {
            let location = touch.locationInNode(self)
            if nodeAtPoint(location) == circle {
                gameSceneDelegate?.scored()
                speedMultiplier += 30
                let randomDegrees = ((Float(random()) / Float(UINT32_MAX)) - 0.5) * 720
                circle.physicsBody?.velocity = CGVectorMake(cos(CGFloat(randomDegrees)) * CGFloat(speedMultiplier), sin(CGFloat(randomDegrees)) * CGFloat(speedMultiplier))
                circle.fillColor = UIColor.greenColor()
                circle.strokeColor = UIColor.greenColor()
                timer = NSTimer.scheduledTimerWithTimeInterval(0.2, target: self, selector: "resetColorOfCircle", userInfo: nil, repeats: false)
            } else {
                gameSceneDelegate?.missed()
                circle.fillColor = UIColor.redColor()
                circle.strokeColor = UIColor.redColor()
                timer = NSTimer.scheduledTimerWithTimeInterval(0.2, target: self, selector: "resetColorOfCircle", userInfo: nil, repeats: false)
            }
        }
    }
    
    func resetColorOfCircle() {
//        circle.fillColor = UIColor.blackColor()
        circle.fillColor = UIColor.darkGrayColor()
        circle.strokeColor = UIColor.clearColor()
    }
    
    func distanceBetweenPoint(point1: CGPoint, point2: CGPoint) -> CGFloat {
        return abs(sqrt((pow(point1.x - point2.x, 2)) + (pow(point1.y - point2.y, 2))))
    }
    
    func reset() {
        circle.physicsBody?.velocity = CGVector(dx: 0, dy: 0)
        circle.position = CGPoint(x: frame.midX, y: frame.midY)
        speedMultiplier = 300
    }
    
    override func update(currentTime: CFTimeInterval) {
    }
}
