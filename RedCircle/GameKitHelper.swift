//
//  GameKitHelper.swift
//  Circle Taps
//
//  Created by Rafael Rincon on 11/4/15.
//  Copyright © 2015 Rafael Rincon. All rights reserved.
//

import GameKit
import UIKit

class GameKitHelper: NSObject {
    
    static var PresentAuthenticationViewController = String()
    
    var AuthenticationViewController = UIViewController()
    var lastError: NSError?
    
    var gameCenterEnabled: Bool
    
    override init() {
        gameCenterEnabled = true
        super.init()
    }
    
    func authenticateLocalPlayer() {
        let localPlayer = GKLocalPlayer.localPlayer()
        
        localPlayer.authenticateHandler = {(vc: UIViewController?, error: NSError?) -> Void in
            self.lastError = error
            print("GameKit Error: \(self.lastError?.userInfo.debugDescription)")
            
            if vc != nil {
                self.AuthenticationViewController = vc!
                NSNotificationCenter.defaultCenter().postNotificationName("PresentAuthenticationViewController", object: self)
            } else if GKLocalPlayer.localPlayer().authenticated {
                self.gameCenterEnabled = true
//                localPlayer.loadDefaultLeaderboardIdentifierWithCompletionHandler({ (leaderboardIdentifier : String!, error : NSError!) -> Void in
//                    if error != nil {
//                        print(error.localizedDescription)
//                    } else {
//                        self.leaderboardIdentifier = leaderboardIdentifier
//                    }
//                })
            } else {
                self.gameCenterEnabled = false
                
            }
        }
        
    }
    
//    static var gameKitHelper: GameKitHelper {
//        get{
//        return self.gameKitHelper
//        }
//        set(newGameKitHelper){
//            self.gameKitHelper = newGameKitHelper
//        }
    //    }
    static var onceToken: dispatch_once_t = dispatch_once_t()
    
//    func sharedGameKitHelper() -> GameKitHelper? {
//        
//        var gameKitHelper: GameKitHelper?
//        
//        dispatch_once(onceToken) {
//            gameKitHelper = GameKitHelper()
//        }
//        
//        return gameKitHelper
//    }
}
