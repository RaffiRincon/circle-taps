//
//  GameViewController.swift
//  RedCircle
//
//  Created by Rafael Rincon on 9/26/15.
//  Copyright (c) 2015 Rafael Rincon. All rights reserved.
//

import UIKit
import SpriteKit
import GameKit

class GameViewController: UIViewController, GameSceneDelegate, GKGameCenterControllerDelegate {

    @IBOutlet weak var missesLeftLabel: UILabel!
    @IBOutlet weak var currentScoreLabel: UILabel!
    @IBOutlet weak var highScoreLabel: UILabel!
    @IBOutlet weak var leaderboardButton: UIButton!
    
    var userDefaults = NSUserDefaults()
    var gameScene: GameScene?
    var gameCenterEnabled = Bool()
    var gameCenterDefaultLeaderBoard = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let oldHighScore = userDefaults.objectForKey("HighScore") as? Int {
            setHighScore(oldHighScore)
        }
        
        print("UIDevice system name = \(UIDevice.currentDevice().localizedModel)\nUIDevice name = \(UIDevice().name)\nUIDevice model = \(UIDevice().model)")
//        struct systemInfo = utsName
//        uname(&systemInfo)
//        let machineName = NSString(CString: systemInfo.machine , encoding: NSUTF8StringEncoding)
//        print("machine name = \(systemInfo)")
        
        
        if let scene = GameScene(fileNamed:"GameScene") {
            
            leaderboardButton.layer.cornerRadius =  leaderboardButton.frame.height / 2
            gameScene = scene
            gameScene?.gameSceneDelegate = self
            //configure the scene
            scene.screenRect = view.frame
            // Configure the view.
            let skView = self.view as! SKView
            skView.showsFPS = false
            skView.showsNodeCount = false
            scene.parentViewController = self
            
            /* Sprite Kit applies additional optimizations to improve rendering performance */
            skView.ignoresSiblingOrder = true
            
            /* Set the scale mode to scale to fit the window */
            scene.scaleMode = .AspectFill
            
            skView.presentScene(scene)
            print("circle width = \(gameScene?.circle.frame.width)")
        }
        
        authenticateLocalPlayer()
    }

    override func viewDidLayoutSubviews() {
        
        var red = CGFloat()
        var green = CGFloat()
        var blue = CGFloat()
        
        view.backgroundColor?.getRed(&red, green: &green, blue: &blue, alpha: nil)
        
        print("red \(red)")
        print("blue \(blue)")
        print("green \(green)")
    }
    override func viewDidAppear(animated: Bool) {
        
    }
    
    override func shouldAutorotate() -> Bool {
        return false
    }

    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        if UIDevice.currentDevice().userInterfaceIdiom == .Phone {
            return .AllButUpsideDown
        } else {
            return .All
        }
    }

    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    @IBAction func onLeaderboardButtonPressed(sender: UIButton) {
        saveHighscore(Int(highScoreLabel.text!)!)
        showLeaderboard()
    }
    
    //MARK:- Custom Funcs -
    
    func setHighScore(score: Int) {
        userDefaults.setValue(score, forKey: "HighScore")
        saveHighscore(score)
        if score / 10 < 1 {
            highScoreLabel.text = "00\(score)"
        } else if score / 100 < 1 {
            highScoreLabel.text = "0\(score)"
        } else {
            highScoreLabel.text = "\(score)"
        }
    }
    
    //MARK: - GameScene Delegation -
    
    func scored() {
        leaderboardButton.hidden = true
        leaderboardButton.enabled = false
        
        let newScore = Int(currentScoreLabel.text!)! + 1
        if newScore > Int(highScoreLabel.text!) {
            userDefaults.setValue(newScore, forKey: "HighScore")
            setHighScore(newScore)
        }
        
        currentScoreLabel.text = "\(newScore)"
    }
    
    func missed() {
        let newMissesLeft = Int(missesLeftLabel.text!)! - 1
        
        if newMissesLeft == 0 {
            let currentHighScore = Int(highScoreLabel.text!)
            let currentScore = Int(currentScoreLabel.text!)
            
            if currentScore > currentHighScore {
                setHighScore(currentScore!)
            }
            
            currentScoreLabel.text = "0"
            missesLeftLabel.text = "5"
            
            leaderboardButton.hidden = false
            leaderboardButton.enabled = true
            
            gameScene?.reset()
        } else {
            missesLeftLabel.text = "\(newMissesLeft)"
        }
    }
    
    //MARK:- Game Center Funcs -
    
    func saveHighscore(score:Int) {
//        var leaderboardID = "Circle_Taps_Leaderboard"
//        var gameKitScore = GKScore(leaderboardIdentifier: leaderboardID)
//        gameKitScore.value = Int(highScoreLabel.text!)!
        
//        if GKLocalPlayer.localPlayer().authenticated {
            let scoreReporter = GKScore(leaderboardIdentifier: "Circle_Taps_Leaderboard") //leaderboard id here
            scoreReporter.value = Int64(score) //score variable here (same as above)
            let scoreArray: [GKScore] = [scoreReporter]
            
            GKScore.reportScores(scoreArray, withCompletionHandler: {(error : NSError?) -> Void in
                if error != nil {
                    print("GameKit error")
                } else {
                    print("score recorded successfully!")
                    print("")
                }
            })
//        }
    }
    
    
    func showLeaderboard() {
//        let vc = self.view?.window?.rootViewController
//        print("viewcontroller = \(vc)")
        let gamCenterViewController = GKGameCenterViewController()
        gamCenterViewController.gameCenterDelegate = self
        gamCenterViewController.viewState = GKGameCenterViewControllerState.Leaderboards
        gamCenterViewController.leaderboardIdentifier = "Circle_Taps_Leaderboard"
//        vc?.presentViewController(gc, animated: true, completion: nil)
        self.presentViewController(gamCenterViewController, animated: true, completion: nil)
    }
    
    
    func gameCenterViewControllerDidFinish(gameCenterViewController: GKGameCenterViewController) {
        gameCenterViewController.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    func authenticateLocalPlayer(){
        let localPlayer = GKLocalPlayer.localPlayer()
        
        localPlayer.authenticateHandler = {(viewController, error) -> Void in
            if viewController != nil {
                self.presentViewController(viewController!, animated: true, completion: nil)
            } else if localPlayer.authenticated {
                self.gameCenterEnabled = true
                localPlayer.loadDefaultLeaderboardIdentifierWithCompletionHandler({ (leaderBoardIdentifier: String?, error: NSError?) -> Void in
                    if error == nil {
                        self.gameCenterDefaultLeaderBoard = leaderBoardIdentifier!
                    } else {
                        print("error 1: \(error)")
                    }
                })
            } else {
                self.gameCenterEnabled = false
                print("Local player could not be authenticated, disabling game center")
                print("error: \(error)")
            }
        }
    }
}
