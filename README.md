# Circle-Taps
Circle Taps is a simple game. 

Here are the rules:  
Tap the circle to get a point.  
Every time you tap the circle, it changes direction and gets faster.  
If you miss 5 times, the game is over.
